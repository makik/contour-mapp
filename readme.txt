ContourmApp

Created for Sarah Titus and Josh Davis on behalf of the Carleton College geology department.
Created by Jon Green, Kiley Maki, and Sam Vinitsky.

This is an app for iOS intended to help students understand and practice concepts related to contour maps and lines, especially the relationship between 2D representations and their 3D counterparts.  
The main activity of this app allows users to draw contour lines of a 2D surface and specify their height, and will create a 3D representation from the input data.
The underlying structure relies on Javascript for 3D rendering, so there is not much Swift code.

The main Javascript file is ContourMapp.js, found in the html/js directory. 
This app utilizes four externally downloaded files: 
	OrbitControls.js handles camera rotation (https://gist.github.com/mrflix/8351020)
	three.min.js is the Three.js library (http://threejs.org/)
	rhill-voronoi-core.min.js is a triangulation library, currently not used, to draw more realistic surfaces (https://github.com/gorhill/Javascript-Voronoi)
	earcut.js is another triangulation library, currently not used, to draw more realistic surfaces (https://github.com/mapbox/earcut)
These files are included in the code for this project.

We attempted to use various methods to improve the way we draw surfaces, but were unable to fully complete one in time for the deadline of this project.
Delauney Triangulation provided us with appropriate triangles to represent surfaces, but tends to draw over and remove gulleys and valleys.
We generated Voronoi diagrams in between contour lines in an attempt to prevent our program from drawing over valleys, but we were unable to produce a surface guarenteed to be free of holes.
earcut is a program to efficiently draw triangulations in 3D, but we found the quality it produced to be unsatisfactory, and it tends to draw surfaces containing holes and with a drastically simplified structure.
The way we currently draw edges is in a 1-to-1 fashion, which tends to produce "twisting", which can be ridiculous if two contours are drawn in opposite directions.
We would suggest, if future developers choose not to use outside libraries exclusively, a method where new faces use functions we build to check if they cross a contour line, then use some behavior of drawing to the nearest point from a given edge to produce a more accurate structure.
It seems possible that some combination of using the internal Voronoi diagram to produce a "spine" and a triangulation algorithm connecting it to nearby contour lines would produce the best results, but we were not able to complete this functionality.

For future development, our code is not well enough documented that someone new to this project could begin immediately, we would recomend they begin by working on documentation first.
The code also has a fair amount of room for improvements to speed and efficiency that we were not able to complete.
Once the provided code has been professionally finished, we believe that it will function as a strong base for simple activities, worksheets, or games involving user generated contour lines or involving the understanding of a provided contour map.
We believe that our work can be used to create effective and user friendly activities to help students learning about contour maps.

Future developers should keep in mind that the bulk of our code refers to 3D points with (x, y, z) coordinates, but the Voronoi-core library takes 2D points in (x, y) format, where the y value of a 2D point refers to the z value of a 3D point.

Here is a potentially helpful link to a list of libraries which will be able to improve the way we draw surfaces.
http://stackoverflow.com/questions/4882993/mesh-generation-from-points-with-x-y-and-z-coordinates