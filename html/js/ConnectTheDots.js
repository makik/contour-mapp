ConnectTheDots = function (){
 
    Button = function(x,y,w,h,color,text){
    //source: http://stackoverflow.com/questions/19840907/draw-rectangle-over-html-with-javascript
    //Position parameters used for drawing the rectangle
        this.x = x;
        this.y = y;
        this.width = w;
        this.height = h;

        this.draw = function(){
            var canvas = document.createElement('canvas'); //Create a canvas element
            //Set canvas width/height
            canvas.style.width='100%';
            canvas.style.height='100%';
            //Set canvas drawing area width/height
            canvas.width = window.innerWidth;
            canvas.height = window.innerHeight;
            //Position canvas
            canvas.style.position='absolute';
            canvas.style.left=0;
            canvas.style.top=0;
            canvas.style.zIndex=100000;
            canvas.style.pointerEvents='none'; //Make sure you can click 'through' the canvas
            document.body.appendChild(canvas); //Append canvas to body element
            var context = canvas.getContext('2d');
            //Draw rectangle
            context.rect(this.x, this.y, this.width, this.height);
            context.fillStyle = color;
            context.fill();
            // Draw text
            context.fillStyle = "black";
            context.font = "12px sans-serif";
            context.fillText(text,x+3,y+3*h/5.,w);
        }
    }
    
    var render = function () {
        requestAnimationFrame( render );
        
        controls.update();
        renderer.render(scene, camera);
    };

    function initialize(){

        // set up the scene
        scene = new THREE.Scene();
        
        renderer.setClearColor( 0x9FBFE4, 1 );

        // make the floor
        scene.add(floorMesh);
        scene.add(camera);
    }

    camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );

    renderer = new THREE.WebGLRenderer();
    controls = new THREE.OrbitControls( camera, renderer.domElement );

    renderer.setSize( window.innerWidth, window.innerHeight );
    document.body.appendChild( renderer.domElement );

    var floor = new THREE.BoxGeometry( 1000000, -0.01, 1000000 );
    var floorMaterial = new THREE.MeshBasicMaterial( { color: 0x5E5E6C } );
    floorMesh = new THREE.Mesh( floor, floorMaterial );
    floorMesh.matrixAutoUpdate = false;

    var hemisphereLight = new THREE.HemisphereLight(0xffffff, 0x5E5E6C, 0.75);
    camera.add(hemisphereLight);

    var directionalLight = new THREE.DirectionalLight(0xffffff, 0.75);
    camera.add(directionalLight);

    var comingSoonButton = new Button(10,10,window.innerWidth/4,window.innerHeight/4,"yellow","COMING SOON");
    comingSoonButton.draw();

    render();
    initialize();
};