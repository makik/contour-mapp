
CONTOUR_DETAIL = 100;

ContourLine = function(pointList, y){

    this.height = y;
    // Raw input points used to construct the line
    this.pointList = [];
    this.pointList2D = [];
    // The average of all points
    this.centerPoint;
    this.centerPoint2D;
    // The curve constructed from the input points
    this.curve;
    this.curve2D;
    // The points extracted from the curve
    this.curvePoints;
    this.curve2DPoints;
    // The edges of the curve
    this.edgeList;
    this.edgeList2D;
    // The line with geometry and material that will be drawn
    this.line;
    this.line2D;
    // The distance around the curve
    this.arcLength;
    // The average distance from each point on the curve to the center point
    this.avgDistToCenter;

    this.parentLine = null;
    this.siblingLines = [];
    this.childLines = [];

    this.constructCurve = function(pointList){
        return new THREE.ClosedSplineCurve3(pointList)
    }

    this.calculateEdges = function(){
        this.edgeList = [];
        this.edgeList2D = [];

        for (var i = 0; i < this.curvePoints.length; i++){
            this.edgeList.push([this.curvePoints[i], this.curvePoints[(i + 1) % this.curvePoints.length]]);
            this.edgeList2D.push([this.curve2DPoints[i], this.curve2DPoints[(i + 1) % this.curve2DPoints.length]]);
        }
    }

    this.getSize = function(){
        var size = 0;
        for(var i = 0; i < this.curvePoints.length; i++){
            size += this.curvePoints[i].distanceTo(this.centerPoint);
        }
        return size/this.curvePoints.length;
    }

    for(var i = 0; i<pointList.length; i++){
        if(!pointList[i].z){
            this.pointList2D[i] = new THREE.Vector3(pointList[i].x,0,pointList[i].y);
            this.pointList[i] = new THREE.Vector3(pointList[i].x,this.height,pointList[i].y);
        }else{
            this.pointList2D[i] = new THREE.Vector3(pointList[i].x,0,pointList[i].z);
            this.pointList[i] = new THREE.Vector3(pointList[i].x,this.height,pointList[i].z);
        }
    }

    this.centerPoint = getCenterPoint(this.pointList);
    this.centerPoint2D = this.centerPoint.clone();
    this.centerPoint2D.y = 0;

    this.curve = this.constructCurve(this.pointList);
    this.curve2D = this.constructCurve(this.pointList2D);

    this.curvePoints = this.curve.getSpacedPoints(CONTOUR_DETAIL);
    this.curve2DPoints = this.curve2D.getSpacedPoints(CONTOUR_DETAIL);

    this.calculateEdges();

    var material = new THREE.LineBasicMaterial( { color : 0x000000, linewidth : 2} );
    
    var geometry = new THREE.Geometry();
    geometry.vertices = this.curvePoints;
    this.line = new THREE.Line(geometry,material);

    geometry = new THREE.Geometry();
    geometry.vertices = this.curve2DPoints;
    this.line2D = new THREE.Line(geometry,material);

    // since we are never moving the lines
    this.line.matrixAutoUpdate = false;
    this.line2D.matrixAutoUpdate = false;

    this.arcLength = this.curve.getLength();
    this.avgDistToCenter = this.getSize();
}

ContourMapp = function (){

    this.contours = [];
    this.topLevelContours = [];
    this.voronoiLines = [];

    var contourControls = new ContourControls(this);

    this.clear = function(){
        this.contours = [];
        this.topLevelContours = [];
        this.voronoiLines = [];
    }

    this.addContour = function(cLine){
        this.contours.push(cLine);
        this.makeHappyFamily(cLine, null, this.topLevelContours);
    }


    this.makeHappyFamily = function(contourLine, parentLine, generation) {
        var test = contourLine.edgeList2D[0];

        for (var i = 0; i < generation.length; i++) {
            if(this.pointContainedInContour(test[0].x, test[0].z, generation[i])){  
                return this.makeHappyFamily(contourLine, generation[i], generation[i].childLines);
            }
        }
        //Contour line is not contained in any of the lines in this generation
        if(parentLine == null) {
            // console.log("Top Level");
            //Forced to rewrite some code for top level stuff
            contourLine.siblingLines = generation.slice();
            var newTopLevelContourList = [];
            for (var i = 0; i < generation.length; i++) {
                //Any lines in this generation that are within the new contour will become its childLines
                test = generation[i].edgeList2D[0];
                if(this.pointContainedInContour(test[0].x, test[0].z, contourLine)){                    
                    //Former siblingLines then lose it as a sibling
                    for(var j = 0; j < generation.length; j++){
                        var pos = generation[j].siblingLines.indexOf(generation[i]);
                        if(pos > -1) {
                            parentLine.childLines[j].siblingLines.splice(pos, 1);
                        }
                    }
                    generation[i].parentLine = contourLine;
                    contourLine.childLines.push(generation[i]);
                    //Meet your new brothers and sisters
                    for(var j = 0; j < contourLine.childLines.length; j++){
                        contourLine.childLines[j].siblingLines.push(generation[i]);
                    }
                    generation[i].siblingLines.push(contourLine.childLines);
                }
                else{
                    generation[i].siblingLines.push(contourLine);
                    newTopLevelContourList.push(generation[i]);
                }
            }
            newTopLevelContourList.push(contourLine);
            this.topLevelContours = newTopLevelContourList;
        }
        else {
            contourLine.siblingLines = generation.slice();
            for (var i = 0; i < generation.length; i++) {

                //Any lines in this generation that are within the new contour will become its childLines
                test = generation[i].edgeList2D[0];
                if(this.pointContainedInContour(test[0].x, test[0].z, contourLine)){
                    //Becomes a grandchild of parentLine, remove it from list of childLines
                    parentLine.childLines.splice(parentLine.childLines.indexOf(generation[i]), 1);
                    //Former siblingLines then lose it as a sibling
                    for(var j = 0; j < parentLine.childLines.length; j++){
                        parentLine.childLines[j].siblingLines.splice(parentLine.childLines[j].siblingLines.indexOf(generation[i]));
                    }

                    generation[i].parentLine = contourLine;
                    contourLine.childLines.push(generation[i]);
                    //Meet your new brothers and sisters
                    for(var j = 0; j < contourLine.childLines.length; j++){
                        contourLine.childLines[j].siblingLines.push(generation[i]);
                    }
                    generation[i].siblingLines.push(contourLine.childLines);
                }
                else{
                    generation[i].siblingLines.push(contourLine);
                }
            }
            //Set parentLine of the new line
            contourLine.parentLine = parentLine;
            parentLine.childLines.push(contourLine);
        }
    }

    //input: list of contours
    //output: list of contours sorted by height
    this.sortContoursByHeight = function(){
        var contours = this.contours;
        // bubble sort!!!
        for (var j = 0; j < contours.length; j++){
            var swapped = false;
            for (var i = 0; i < contours.length - 1; i++){
                if (contours[i].height > contours[i+1].height){
                    var temp = contours[i];
                    contours[i] = contours[i+1];
                    contours[i+1] = temp;
                    swapped = true;
                }
            }
            if(!swapped){
                break;
            }
        }
        return contours;
    }

    this.sortContoursBySize = function(){
        var contours = this.contours;
        for (var j = 0; j < contours.length; j++){
            var swapped = false;
            for (var i = 0; i < contours.length - 1; i++){
                if (contours[i].arcLength < contours[i+1].arcLength){
                    var temp = contours[i];
                    contours[i] = contours[i+1];
                    contours[i+1] = temp;
                    swapped = true;
                }
            }
            if(!swapped){
                break;
            }
        }
        return contours;
    }

    this.doContoursOverlap = function(c1,c2){
        var center1 = c1.centerPoint2D;
        var center2 = c2.centerPoint2D;
        center1.y = 0;
        center2.y = 0;

        var raycaster = new THREE.Raycaster();
        raycaster.linePrecision = 0;

        // Raycast from center of c2 to every point in c1, if it intersects c1 an odd number of times return true
        var count1 = 0.;
        for(var i = 0; i<c1.curve2DPoints.length; i++){
            var edgePoint = c1.curve2DPoints[i];
            raycaster.set(center2,edgePoint);

            var intersects = raycaster.intersectObject(c1.line2D,false);
            if(intersects.length % 2 == 1){
                return true;
            }
        }

        // Do the same, but the other way around
        var count2 = 0.;
        for(var i = 0; i<c2.curve2DPoints.length; i++){
            var edgePoint = c2.curve2DPoints[i];
            raycaster.set(center1,edgePoint);

            var intersects = raycaster.intersectObject(c2.line2D,false);
            if(intersects.length % 2 == 1){
                return true;
            }
        }

        return false;
    }

    this.pointContainedInContour = function(x, y, contour){
        var edgeList = contour.edgeList;
        var interceptCount = 0;
        for (var i = 0; i < edgeList.length; i++){
            if (edgeList[i][0].x <= x && x <= edgeList[i][1].x || edgeList[i][0].x >= x && x >= edgeList[i][1].x){
                var slope = (edgeList[i][1].z - edgeList[i][0].z) / (edgeList[i][1].x - edgeList[i][0].x);
                var b = edgeList[i][0].z - (slope * edgeList[i][0].x);
                if (x * slope + b > y) {
                    interceptCount += 1;
                }
            }
        }
        if(interceptCount % 2 == 1){
            return true;
        }
        return false;
    }

    this.lineCrossesContour = function(p1, p2, contour){
        if(!p1.z){
            p1 = new THREE.Vector3(p1.x,0,p1.y);
        }
        if(!p2.z){
            p2 = new THREE.Vector3(p2.x,0,p2.y);
        }
        var edgeList = contour.edgeList2D;
        for (var i = 0; i < edgeList.length - 1; i++){
            var edge = edgeList[i]
            var q1 = edge[0];
            var q2 = edge[1];
            if (doLineSegmentsCross(p1, p2, q1, q2)){
                return true;
            }
        }
        return false;
    }

    // see if this line crosses any contours
    this.lineCrossesContours = function(p1, p2){
        for (var i = 0; i < this.contours.length; i++){
            if(this.lineCrossesContour(p1, p2, this.contours[i])){
                return true;
            }
        }
        return false;
    }

    // see if this line crosses any contours
    this.voronoiLineCrossesContours = function(line){
        for (var i = 0; i < this.contours.length; i++){
            if(this.lineCrossesContour(line.va, line.vb, this.contours[i])){
                return true;
            }
        }
        return false;
    }

    // determines if this line lies entirely outside the contours
    this.voronoiLineOutsideAllContours = function(line){
        for (var i = 0; i < this.contours.length; i++){
            if(this.pointContainedInContour(line.va.x, line.va.y, this.contours[i])){
                if(this.pointContainedInContour(line.vb.x, line.vb.y, this.contours[i])){  
                    return false;
                }
            }
        }
        return true;
    }

    // input: list of contour lines
    // output: list of (list contour lines), where each mega-list is a stack 
    this.splitIntoStacks = function(){
        
        // list of stacks (which are in turn lists!)
        var contourStackList = [];
        
        var contoursSortedBySize = this.sortContoursBySize(this.contours);

        // var contoursSortedByHeight = sortContoursByHeight(contours);

        for (var i = 0; i < contoursSortedBySize.length; i++){

            var stackedThisContour = false;
            var thisContour = contoursSortedBySize[i];

            // we assume you're only on top of ONE stack
            // see if this contour is above the top of any other stack
            for (var j = 0; j < contourStackList.length; j++){
                
                var thisStack = contourStackList[j];
                var topOfStack = thisStack[thisStack.length-1];

                if (this.doContoursOverlap(topOfStack,thisContour) && !stackedThisContour){
                    thisStack.push(thisContour);
                    stackedThisContour = true;
                    break;
                }
            }

            // if this contour doesn't belong on some other stack, put it on a new one
            if (!stackedThisContour){
                var newStack = [];
                for(var j = 0; j<i; j++){
                    if(this.doContoursOverlap(contoursSortedBySize[j],thisContour)){
                        newStack.push(contoursSortedBySize[j]);
                    }
                }
                newStack.push(thisContour);
                contourStackList.push(newStack);
            }
        }

        return contourStackList;
    }

    this.addVoronoiDiagrams = function(contours){

        for (var i = 0; i < contours.length; i++) {
            var len = contours[i].childLines.length
            for (var j = 0; j < len; j++) {
                this.addVoronoiDiagrams(contours[i].childLines[j]);
            }

            var voronoi = this.voronoiDiagram(contours[i]);

            var height = contours[i].childLines.length == 0 ? contours[i].height : (contours[i].height+contours[i].childLines[0].height)/2.;

            cL = new ContourLine(voronoi.pointList, height);
            
            cL.edgeList2D = [];
            for (var j =0; j < voronoi.edgeList.length; j++){
                cL.edgeList2D.push(new THREE.Vector3(voronoi.edgeList[j].va.x, 0, voronoi.edgeList[j].va.y), new THREE.Vector3(voronoi.edgeList[j].vb.x, 0, voronoi.edgeList[j].vb.y));
            }            
            
            cL.parentLine = contours[i];
            cL.childLines = [];
            len = contours[i].childLines.length
            for(var j = 0; j < len; j++){
                contours[i].childLines[j].siblingLines.push(cL);
            }
            contours[i].childLines.push(cL);

            this.voronoiLines.push(cL);
        }
    }

    this.drawEdgesToPoints = function(geometry, contour, pointList){
        var edgeList = contour.edgeList;
        for (var i = 0; i < edgeList.length; i++){
            var seperatedPoints = this.splitPointsByEdge(edgeList[i], pointList);

            this.drawEdgeToClosestPoint(geometry, edgeList[i], seperatedPoints[0]);
            this.drawEdgeToClosestPoint(geometry, edgeList[i], seperatedPoints[1]);
        }
    }

    this.drawEdgeToClosestPoint = function(geometry, edge, pointList){
        var minDist = Infinity;
        var minPoint = null;
        var dist;
        for(var i = 0; i < pointList.length; i++){
            dist = Math.pow(edge[0].distanceTo(pointList[i]), 2) + Math.pow(edge[1].distanceTo(pointList[i]), 2);
            if (dist < minDist){
                minDist = dist;
                minPoint = pointList[i];
            }
        }
        if(minPoint != null){
            addTriangle(geometry, minPoint, edge[0], edge[1]);
        }
    }

    this.splitPointsByEdge = function(edge, pointList){
        var pointList1 = [];
        var pointList2 = [];
        for (var i = 0; i < pointList.length; i++){
            var slope = (edge[1].z - edge[0].z) / (edge[1].x - edge[0].x);
            var b = edge[0].z - (slope * edge[0].x);
            if (pointList[i].x * slope + b > pointList[i].z) {
                pointList1.push(pointList[i]);
            }
            else {
                pointList2.push(pointList[i]);
            }
        }

        return [pointList1, pointList2];
    }

    // returns a list of triples (triangles)
    this.triangulateContours = function(){
        var triangleList = [];
        var triangleTuple = [];
        
        var pointList = []; // flat list of points
        var pointListFlat = []; // flat list of points
        var holeIndices = []; // list of indices form pointList that make holes
        var thisContour;
        
        for (var i = 0; i < this.contours.length ; i++){
            
            thisContour = this.contours[i];
            pointListFlat = [];
            pointList = [];
            holeIndices = [];

            // make list of points in this
            for (var j = 0; j < thisContour.curvePoints.length; j++){

                pointListFlat.push(thisContour.curvePoints[j].x);
                pointListFlat.push(thisContour.curvePoints[j].z)
                pointListFlat.push(thisContour.curvePoints[j].y)

                pointList.push(thisContour.curvePoints[j]);
            }

            // add all the children's points
            for (var l = 0; l < thisContour.childLines.length; l++){

                for (var j = 0; j < thisContour.childLines[l].curvePoints.length; j++){

                    pointListFlat.push(thisContour.childLines[l].curvePoints[j].x);
                    pointListFlat.push(thisContour.childLines[l].curvePoints[j].z)
                    pointListFlat.push(thisContour.childLines[l].curvePoints[j].y)

                    pointList.push(thisContour.childLines[l].curvePoints[j]);
                }
            }

            var triangles = earcut(pointListFlat, null, 3);

            var index1;
            var index2;
            var index3;
            var p1;
            var p2;
            var p3;

            for (var k = 0; k < triangles.length; k += 3){
                index1 = triangles[k];
                index2 = triangles[k+1];
                index3 = triangles[k+2];
                p1 = pointList[index1];
                p2 = pointList[index2];
                p3 = pointList[index3];

                triangleTuple = [p1,p2,p3];
                triangleList.push(triangleTuple);
            }
        }

        return triangleList;
    }

    // calculates voronoi diagram of the entire scene
    this.voronoiDiagram = function(contour){
        var voronoi = new Voronoi();
        var bbox = {xl: -1000, xr: 1000, yt: -1000, yb: 1000};
        
        var pointList = [];
        // add every contour

        for (var i = 0; i < contour.curvePoints.length; i++) {
            pointList.push({x: contour.curve2DPoints[i].x, y: contour.curve2DPoints[i].z});
        }
        for (var i = 0; i < contour.childLines.length; i++){
            for (var j = 0; j< contour.childLines[i].curve2DPoints.length; j++){
                pointList.push({x: contour.childLines[i].curve2DPoints[j].x, y: contour.childLines[i].curve2DPoints[j].z})
            }
        }

        var diagram = voronoi.compute(pointList, bbox);

        var internalVoronoiEdges = [];
        var internalVoronoiPoints = [];
        for (i = 0; i < diagram.edges.length; i++) {
            // if its not on the outside
            if (!this.voronoiLineOutsideAllContours(diagram.edges[i], this.contours)){
                // if it doesn't cross any contours
                if (!this.voronoiLineCrossesContours(diagram.edges[i], this.contours)){
                    internalVoronoiEdges.push(diagram.edges[i]);
                    if(internalVoronoiPoints.indexOf(diagram.edges[i].va) == -1){
                        internalVoronoiPoints.push(diagram.edges[i].va);
                    }
                    if(internalVoronoiPoints.indexOf(diagram.edges[i].vb) == -1){
                        internalVoronoiPoints.push(diagram.edges[i].vb);
                    }
                }
            }
        }

        var material = new THREE.LineBasicMaterial({color: 0xff0000});
        var height = 0;
        var edgeList = [];

        for (i = 0; i < internalVoronoiEdges.length; i++) {

            heightA = this.getHeightOfVoronoiPoint(internalVoronoiEdges[i].va, this.contours);
            heightB = this.getHeightOfVoronoiPoint(internalVoronoiEdges[i].vb, this.contours);

            var geometry = new THREE.Geometry();
            v1 = new THREE.Vector3(internalVoronoiEdges[i].va.x, heightA, internalVoronoiEdges[i].va.y);
            v2 = new THREE.Vector3(internalVoronoiEdges[i].vb.x, heightB, internalVoronoiEdges[i].vb.y);
            geometry.vertices.push(v1);
            geometry.vertices.push(v2);
            var line = new THREE.Line(geometry, material);
            scene.add(line);
            edgeList.push({v1: v1, v2: v2});
        }

        return {'edgeList':internalVoronoiEdges, 'pointList':internalVoronoiPoints};
    }

    // figure out the height of this voronoi point
    this.getHeightOfVoronoiPoint = function(point){

        // find contour directly below it
        var contoursSortedByHeight = this.sortContoursByHeight(this.contours);
        // tallest contour containing this point
        var tallestContourIndex = 0;
        for (var i = 0; i < contoursSortedByHeight.length; i++){
            if (this.pointContainedInContour(point.x,point.y,contoursSortedByHeight[i])){
                tallestContourIndex = i;
            }
        }

        var height = contoursSortedByHeight[tallestContourIndex].height;

        return height;
    }

    // uses voronoi information to draw the surface
    this.getSurfaceMeshVoronoi = function(){    
        var faceGeom = new THREE.Geometry();
        this.addVoronoiDiagrams(this.topLevelContours);

        this.generateSurface(this.topLevelContours, faceGeom);

        // voronoiPointList is a list of all points in voronoi lines
        // contourPointList is a list of all points in contour lines

        for(var i = 0; i < this.voronoiLines; i++){
            var vLine = this.voronoiLines[i];
            for(var j = 0; j<this.contours.length; j++){
                var cLine = this.contours[j];
                cLine.childLines.splice(cLine.childLines.indexOf(vLine),1);
                cLine.siblingLines.splice(cLine.childLines.indexOf(vLine),1);
            }
        }

        this.voronoiLines = [];

        return makeMesh(faceGeom);
    }

    this.generateSurface = function(contours, geometry){
        var edgeList;
        var pointList;
        var seperatedPoints;
        for(var i = 0; i < contours.length; i++){
            pointList = [];

            if(contours[i].parentLine != null) {
                pointList = pointList.concat(contours[i].parentLine.curvePoints);
            }
            for(var j = 0; j < contours[i].siblingLines.length; j++){
                pointList = pointList.concat(contours[i].siblingLines[j].curvePoints);
            }
            for(var j = 0; j < contours[i].childLines.length; j++){
                pointList = pointList.concat(contours[i].childLines[j].curvePoints);
            }

            this.drawEdgesToPoints(geometry, contours[i], pointList);

            this.generateSurface(contours[i].childLines, geometry);
        }   
    }

    // uses triangulation to figure out which triangles to draw
    this.getSurfaceMeshTrianglation = function(){

        var faceGeom = new THREE.Geometry();
        
        var triangleList = this.triangulateContours();

        var triangle;
        for (var i = 0; i < triangleList.length; i++){
            triangle = triangleList[i];
            addTriangle(faceGeom, triangle[0],triangle[1],triangle[2]);
        }

        return makeMesh(faceGeom);

    }

    // takes as input list contours (lists of points)
    this.getSurfaceMesh = function(){

        var faceGeom = new THREE.Geometry();

        // split all the contours into seperate stacks
        var contourStackList = this.splitIntoStacks(this.contours);

        for (var k = 0; k < contourStackList.length; k++){

            var singleContourStack = contourStackList[k];

            for (var i = 0; i < singleContourStack.length - 1; i++){

                this.drawBetweenContours(faceGeom, singleContourStack[i],singleContourStack[i+1]);

                // this.drawEdgesToPoints(faceGeom, singleContourStack[i], singleContourStack[i+1].curvePoints);
            }

            var topContourInStack = singleContourStack[singleContourStack.length -1];
            var peak = topContourInStack.centerPoint;
            for(var i = 0; i < topContourInStack.curvePoints.length - 1; i++) {
                var p1 = topContourInStack.curvePoints[i];
                var p2 = topContourInStack.curvePoints[i+1];
                
                addTriangle(faceGeom, p1, p2, peak);
            }
        }

        return makeMesh(faceGeom);
    }

    this.drawBetweenContours = function(geometry, c1, c2){

        for(var i = 0; i < c1.curvePoints.length - 1; i++){
            var p1 = c1.curvePoints[i];
            var p2 = c1.curvePoints[i+1];
            var p3 = c2.curvePoints[i];
            var p4 = c2.curvePoints[i+1];

            addTriangle(geometry, p3,p2,p1);
            addTriangle(geometry, p2,p3,p4);
        }
    }
}


function makeMesh(geom){
    geom.computeFaceNormals();
    geom.computeVertexNormals();
    
    var material = new THREE.MeshPhongMaterial( { color: 0x6B5A38, side: THREE.DoubleSide });

    var object = new THREE.Mesh(geom, material );

    // speed up performance
    object.matrixAutoUpdate = false;

    return object;
}

function addTriangle(geom, v1, v2, v3) {
    var l = geom.vertices.length;

    geom.vertices.push(v1);
    geom.vertices.push(v2);
    geom.vertices.push(v3);
    var face = new THREE.Face3(l, l+1, l+2);

    geom.faces.push(face);
}

function getCenterPoint(pointList){
    var count = 0;
    var xSum = 0;
    var ySum = 0;
    var zSum = 0;
    for(var i = 0; i<pointList.length; i++){
        count += 1;
        xSum += pointList[i].x;
        ySum += pointList[i].y;
        zSum += pointList[i].z;
    }
    if (count == 0) {
        return new THREE.Vector3(0.0,0.0,0.0);
    }
    return new THREE.Vector3(xSum/count, ySum/count, zSum/count);
}

// determines if the line segment from a to b crosses the one from c to d
// c and d are Vector3's
// http://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
function doLineSegmentsCross(a, b, c, d){

    var Ax = a.x;
    var Ay = a.z;
    var Bx = b.x;
    var By = b.z;

    var Cx = c.x;
    var Cy = c.z;
    var Dx = d.x;
    var Dy = d.z;

    // E = B - A
    var Ex = Bx - Ax;
    var Ey = By - Ay;
    // F = D - C
    var Fx = Dx - Cx;
    var Fy = Dy - Cy;
    // R = A - C
    var Rx = Ax - Cx;
    var Ry = Ay - Cy;
    // S = C - A
    var Sx = Cx - Ax;
    var Sy = Cy - Ay;

    // P * E = 0
    var Px = -1 * Ey;
    var Py = Ex;
    // Q * F = 0
    var Qx = -1 * Fy;
    var Qy = Fx;

    // R dot P 
    var RP = (Rx * Px) + (Ry * Py);
    // F dot P
    var FP = (Fx * Px) + (Fy * Py);
    // S dot Q
    var SQ = (Sx * Qx) + (Sy * Qy);
    // E dot Q
    var EQ = (Ex * Qx) + (Ey * Qy);

    // parallel
    if ((FP == 0) || (EQ == 0)){
        return false;
    }

    // (R dot P) / (F dot P)
    var h = RP / FP;
    // (S dot Q) / (E dot Q)
    var g = SQ / EQ;

    if ((0 < g && g < 1) && (0 < h && h < 1)){

        return true;
    }

    return false;
}

// takes a list of points (vector3's)
// returns this list sorted in counterclockwise order
function sortPointsInCounterClockwiseOrder(pList){
   
    var pointList = pList;

    // dynamic mergesort
    // size of our merge subblock
    var size = 1;
    var length = pointList.length;

    var index = 0; // index of the current item
    
    var blockIndexL = 0; // index of the left block
    var blockIndexR = 1; // index of the right block

    var mergeIndexL = 0;
    var mergeIndexR = 0;

    var blockLengthL = 1; // size
    var blockLengthR = 1; // if length > 2 * size, this = size. Else = length - size

    var mergedPointList = [];
    var blockPointList = [];

    var centerPoint = getCenterPoint(pointList);

    // do this until we have a block size greater than the size of the list
    while (size <= length){

        index = 0;
        blockPointList = [];

        // do this for every block 
        while(index < length){

            blockIndexL = index;
            blockIndexR = index + size;

            mergeIndexL = 0;
            mergeIndexR = 0;

            mergedPointList = [];

            blockLengthL = size;
            blockLengthR = size;

            if (blockIndexL + blockLengthL > length){
                blockLengthL = length%size;
                blockLengthR = 0;
            }else if (blockIndexR + blockLengthR >= length){
                blockLengthR = length % size; // what's left over
            }

            while(mergeIndexL < blockLengthL || mergeIndexR < blockLengthR){

                // if both are still in the list
                if (mergeIndexL < blockLengthL && mergeIndexR < blockLengthR){

                    leftPoint = pointList[(blockIndexL + mergeIndexL) % pointList.length];
                    rightPoint = pointList[(blockIndexR + mergeIndexR) % pointList.length];

                    // sort in ascending order
                    if(isCounterClockwiseOf(leftPoint, rightPoint, centerPoint)){
                        mergedPointList.push(leftPoint);
                        mergeIndexL += 1;
                    } else {
                        mergedPointList.push(rightPoint);
                        mergeIndexR += 1;
                    }

                } else if (mergeIndexL < blockLengthL){ // still have stuff in L list
                    leftPoint = pointList[(blockIndexL + mergeIndexL) % pointList.length];

                    mergedPointList.push(leftPoint);
                    
                    mergeIndexL += 1;
                } else if (mergeIndexR < blockLengthR){
                    rightPoint = pointList[(blockIndexR + mergeIndexR) % pointList.length];

                    mergedPointList.push(rightPoint);
                    
                    mergeIndexR += 1;
                } else {
                    // window.alert();
                }
            }

            // add the next element of merged list to blockList
            for (var i = 0; i < mergedPointList.length; i++){
                blockPointList.push(mergedPointList[i]);
            }

            index += 2 * size; //move two blocks at a time
        }

        //double the size
        size *= 2;

        pointList = blockPointList;
    }
    return pointList; 
}

// Returns true if a is counterclockwise of b, starting with 'noon'
// Source: http://stackoverflow.com/questions/6989100/sort-points-in-clockwise-order
function isCounterClockwiseOf(a, b, center){

    if (a.x - center.x >= 0 && b.x - center.x < 0){
        return true;
    }
    if (a.x - center.x < 0 && b.x - center.x >= 0){
        return false;
    }
    if (a.x - center.x == 0 && b.x - center.x == 0) {
        if (a.z - center.z >= 0 || b.z - center.z >= 0){
            return a.z > b.z;
        }
        return b.z < a.z;
    }

    // compute the cross product of vectors (origin -> a) x (origin -> b)
    var det = (a.x - center.x) * (b.z - center.z) - (b.x - center.x) * (a.z - center.z);
    if (det > 0){
        return false;
    }else if (det < 0){
        return true;
    }

    // points a and b are on the same line from the center
    // check which point is closer to the center
    var d1 = (a.x - center.x) * (a.x - center.x) + (a.z - center.z) * (a.z - center.z);
    var d2 = (b.x - center.x) * (b.x - center.x) + (b.z - center.z) * (b.z - center.z);
    return d1 > d2;
}