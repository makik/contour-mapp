
CAMERA_2D_HEIGHT = 15;

ContourControls = function(contourMap){

    var selfCopy = this;
    
    this.drawing = false;
    this.newPointList = [];
    this.cMap = contourMap;

    this.make2D = function(){
        infoButton.setText("  2D Drawing Mode");
        mode = "2D";

        this.initializeScene();

        camera.position.set(0,CAMERA_2D_HEIGHT,0);
        camera.lookAt(new THREE.Vector3(0,0,0));

        controls.target = new THREE.Vector3(0,0,0);
        controls.enabled = false;

        this.addContoursToScene(this.cMap.contours);
    }

    this.make3D = function(){
        // console.log("I got here - make 3D");
        infoButton.setText("  3D Viewing Mode");
        mode = "3D";

        this.initializeScene();

        var contours = this.cMap.contours;

        var m = 0;
        var listOfCenterPoints = [];
        for (var i = 0; i < contours.length; i++){
            if (contours[i].height > m){
                m = contours[i].height;
            }
            listOfCenterPoints.push(contours[i].centerPoint);
        }

        // move the camera to be 3D
        camera.position.set(15,m * 1.5,15);
        var target = getCenterPoint(listOfCenterPoints);
        camera.lookAt(target);

        controls.target = target;
        controls.enabled = true;

        var faceMesh = this.cMap.getSurfaceMesh();
        scene.add(faceMesh);
        
        this.addContoursToScene(this.cMap.contours);
    }

    this.toggle3D = function(){
        if (mode == "2D" && this.cMap.contours.length > 0){
            this.make3D();
        } else {
            this.make2D();
        }
    }

    this.createContour = function(height){
        var convertedPointList = convertPointList(this.newPointList, false);

        var s = new Smoother();
        s.smoothListEnds(convertedPointList,1);

        var c = new ContourLine(convertedPointList,height);
        this.cMap.addContour(c);
        this.addContourToScene(c);
    }

    this.addContoursToScene = function(cLines){
        // draw each contour line
        for(var i = 0; i<cLines.length; i++){
            this.addContourToScene(cLines[i]);
        }
    }

    this.addContourToScene = function(cLine){
        scene.remove(cLine.line2D);
        scene.remove(cLine.line);
        if(mode == "2D"){
            scene.add(cLine.line2D);
        }else{
            scene.add(cLine.line);
        }
    }

    this.clear = function(){
        // check to make sure
        if (!window.confirm("Are you sure you want to clear the screen?")){
            return;
        }

        this.cMap.clear();
        this.make2D();
        render();
    }

    // To: Whoever reads this part of the code
    // From: Kiley Maki
    //
    // I'm sorry.
    //
    // P.S. I did this because the "this" keyword doesn't work when
    // a method in an object is called as an event listener. This
    // is a wraparound. An unholy wraparound.
    // 
    // P.P.S. Javascript pls
    this.onMouseDown = function(event){
        selfCopy.handleMouseDown(event.clientX,event.clientY);
    }

    this.onMouseMove = function(event){
        selfCopy.handleMouseMove(event.clientX,event.clientY);
    }

    this.onMouseUp = function(event){
        selfCopy.handleMouseUp(event.clientX,event.clientY);
    }

    this.handleMouseDown = function(x, y){
        if(this.handleButtonClick(x, y)){
            return;
        }

        if (mode != "2D"){return;}

        this.drawing = true;
        this.newPointList = [];

        document.addEventListener('mousemove',this.onMouseMove,false);
        document.addEventListener('mouseup',this.onMouseUp,false);
    }

    this.handleMouseMove = function(x, y){
        if (mode != "2D"){return;}

        var newPoint = new THREE.Vector2(x,y);

        if(this.newPointList.length > 1){
            var prevPoint = this.newPointList[this.newPointList.length-1];
            this.drawMouseTraceOverlay(prevPoint.x,prevPoint.y,newPoint.x,newPoint.y);
        }else{
            this.drawMouseTraceOverlay(newPoint.x,newPoint.y,newPoint.x,newPoint.y);
        }

        this.newPointList.push(newPoint);
    }

    this.handleMouseUp = function(x, y){
        if (mode != "2D"){return;}

        if(this.newPointList.length<=1){
            this.endDraw();
            return;
        }

        if (this.drawing == true){
            var newPoint = new THREE.Vector2(x,y);
            this.newPointList.push(newPoint);

            var h = window.prompt("Please enter the height of this contour (between 0 and 100):");
            if(!h){
                this.endDraw();
                return;
            }
            h = parseInt(h);
            h = Math.min(100,Math.max(0,h));

            this.createContour(h);
            this.endDraw();
            render();
        }
    }

    this.handleButtonClick = function(x, y){
        // if we clicked the toggle button
        if (window.innerWidth/2 - 75 < x && x < window.innerWidth/2 + 75 &&
            10 < y && y < 70){
            this.toggle3D();
            return true;
        }
        // if we clicked the clear button
        if (10 < x && x < 70 &&
            10 < y && y < 70){
            this.clear();
            return true;
        }

        return false;
    }

    this.drawMouseTraceOverlay = function(prevX,prevY,x,y){
        var canvas = this.getMouseOverlayCanvas();

        var context = canvas.getContext('2d');
        context.beginPath();
        context.moveTo(prevX,prevY);
        context.lineTo(x,y);
        context.stroke();
    }

    this.getMouseOverlayCanvas = function(){
        var canvas=document.getElementById("overlayCanvas");
        if(!canvas){
            canvas = document.createElement("canvas");
            canvas.setAttribute("id", "overlayCanvas");
            //Set canvas width/height
            canvas.style.width='100%';
            canvas.style.height='100%';
            //Set canvas drawing area width/height
            canvas.width = window.innerWidth;
            canvas.height = window.innerHeight;
            //Position canvas
            canvas.style.position='absolute';
            canvas.style.left=0;
            canvas.style.top=0;
            canvas.style.zIndex=100000;
            canvas.style.pointerEvents='none'; //Make sure you can click 'through' the canvas
            document.body.appendChild(canvas); //Append canvas to body element
        }
        return canvas;
    }

    this.endDraw = function(shouldKeepLine){
        this.drawing = false;
        this.newPointList = [];

        document.removeEventListener('mousemove',this.onMouseMove,false);
        document.removeEventListener('mouseup',this.onMouseUp,false);
        
        var canvas = document.getElementById("overlayCanvas");
        if(canvas){
            canvas.parentNode.removeChild(canvas);
        }
    }

    this.initializeScene = function(){
        // set up the scene
        scene = new THREE.Scene();

        // make the floor
        scene.add(floorMesh);
        scene.add(camera);
    }

    document.addEventListener('mousedown',this.onMouseDown,false);

    camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );

    // source: https://github.com/mrdoob/three.js/issues/1239
    visHeight = 2.*Math.tan((camera.fov*Math.PI/180)/2.)*CAMERA_2D_HEIGHT;
    hFOV = 2.*Math.atan(Math.tan((camera.fov*Math.PI/180)/2.)*camera.aspect);
    visWidth  = 2.*Math.tan(hFOV/2.)*CAMERA_2D_HEIGHT;

    renderer = new THREE.WebGLRenderer();
    controls = new THREE.OrbitControls( camera, renderer.domElement );

    renderer.setSize( window.innerWidth, window.innerHeight );
    renderer.setClearColor( 0x9FBFE4, 1 );

    document.body.appendChild( renderer.domElement );

    var floor = new THREE.BoxGeometry( 1000, -0.01, 1000 );
    var floorMaterial = new THREE.MeshBasicMaterial( { color: 0x006400 } );
    floorMesh = new THREE.Mesh( floor, floorMaterial );
    floorMesh.matrixAutoUpdate = false;

    var hemisphereLight = new THREE.HemisphereLight(0xffffff, 0x5E5E6C, 0.75);
    camera.add(hemisphereLight);

    var directionalLight = new THREE.DirectionalLight(0xffffff, 0.75);
    camera.add(directionalLight);

    var clearButton = new Button(10,10,60,60,"red",12,2);
    clearButton.draw(" Clear All");

    infoButton = new Button(window.innerWidth/2 - 75,10,150,60,"yellow", 16,4);

    initTouchHandlers();
    this.initializeScene();
    this.make2D();
    render();
}

///////////////////
//  HELPER STUFF //
///////////////////

Button = function(x,y,w,h,color,textSize,borderWidth){
//source: http://stackoverflow.com/questions/19840907/draw-rectangle-over-html-with-javascript
    
    //Position parameters used for drawing the rectangle
    this.x = x;
    this.y = y;
    this.width = w;
    this.height = h;

    this.textSize = textSize;
    this.borderWidth = borderWidth;

    var canvas = document.createElement('canvas'); //Create a canvas element
    //Set canvas width/height
    canvas.style.width='100%';
    canvas.style.height='100%';
    //Set canvas drawing area width/height
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    //Position canvas
    canvas.style.position='absolute';
    canvas.style.left=0;
    canvas.style.top=0;
    canvas.style.zIndex=100000;
    canvas.style.pointerEvents='none'; //Make sure you can click 'through' the canvas
    document.body.appendChild(canvas); //Append canvas to body element

    this.canvas = canvas;

    this.draw = function(text){
        var context = this.canvas.getContext('2d');
        
        //Draw rectangle
        context.rect(this.x, this.y, this.width, this.height);
        context.fillStyle = color;
        context.fill();
        
        // Draw the border
        context.lineWidth = this.borderWidth;
        context.strokeStyle = 'black';
        context.stroke();

        // Draw text
        context.fillStyle = "black";
        context.font = this.textSize+"px sans-serif";
        context.fillText(text,x+3,y+3*h/5.,w);
    }

    this.setText = function(text){
        var context = this.canvas.getContext('2d');
        context.beginPath();
        context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.draw(text);
    }
}

Smoother = function(){
    // Change smoothing to be an object
    this.prevInputsX = [];
    this.prevInputsZ = [];
    this.SMOOTHING_ORDER = 4;

    this.smoothPoint = function(point){
        var x = point.x;
        var z = point.z;
        if(this.prevInputsX.length < this.SMOOTHING_ORDER){
            this.prevInputsX.push(x);
            this.prevInputsZ.push(z);
            return point;
        }

        this.prevInputsX.splice(0,1);
        this.prevInputsX.push(x);

        this.prevInputsZ.splice(0,1);
        this.prevInputsZ.push(z);

        sumX = 0.;
        sumZ = 0.;
        for(var i = 0; i<this.SMOOTHING_ORDER; i++){
            sumX += this.prevInputsX[i];
            sumZ += this.prevInputsZ[i];
        }

        return new THREE.Vector3(this.sumX/this.SMOOTHING_ORDER,0,this.sumZ/this.SMOOTHING_ORDER);
    }

    this.smoothList = function(list, iterations){
        if(!iterations) iterations = 1;

        this.prevInputsX = [];
        this.prevInputsZ = [];
        for(var i = 0; i<iterations; i++){
            for(var j = 0; j<list.length; j++){
                list[j] = this.smoothPoint(list[j]);
            }
        }
        // console.log(list);
        return list;
    }

    this.smoothListEnds = function(list, iterations){
        this.prevInputsX = [];
        this.prevInputsZ = [];

        var cutoff = Math.ceil(this.SMOOTHING_ORDER/2.);

        for(var j = 0; j<iterations; j++){
            for(var i = list.length - cutoff; i != cutoff; i = (i+1)%list.length){
                this.smoothPoint(list[i]);
            }
        }
    }
}

function drawTestLine2(x,z){
    var testGeom = new THREE.Geometry();
    testGeom.vertices.push(new THREE.Vector3(x,0,z),new THREE.Vector3(x,100,z));
    scene.add(new THREE.Line(testGeom,new THREE.LineBasicMaterial({color:0x0000ff})));
}

function drawTestLine(point){
    drawTestLine2(point.x,point.z);
}

function drawTestLines(points){
    for(var i = 0; i<points.length; i++){
        drawTestLine(points[i]);
    }
}

function convertPoint(point, reversed){
    var newPoint;
    if(!reversed){
        newPoint = new THREE.Vector3((point.x/window.innerWidth-0.5)*visWidth,0,(point.y/window.innerHeight-0.5)*visHeight);
    }else{
        newPoint = new THREE.Vector2((point.x/visWidth+0.5)*window.innerWidth,(point.z/visHeight+0.5)*window.innerHeight);
    }
    point = newPoint;
    return point;
}

function convertPointList(pointList, reversed){
    for(var i = 0; i<pointList.length; i++){
        pointList[i] = convertPoint(pointList[i],reversed);
    }
    return pointList;
}

// http://stackoverflow.com/questions/1517924/javascript-mapping-touch-events-to-mouse-events
function touchHandler(event)
{
    var touches = event.changedTouches,
        first = touches[0],
        type = "";
    switch(event.type)
    {
        case "touchstart": type = "mousedown"; break;
        case "touchmove":  type = "mousemove"; break;        
        case "touchend":   type = "mouseup";   break;
        default:           return;
    }

    var simulatedEvent = document.createEvent("MouseEvent");
    simulatedEvent.initMouseEvent(type, true, true, window, 1, 
                                  first.screenX, first.screenY, 
                                  first.clientX, first.clientY, false, 
                                  false, false, false, 0/*left*/, null);

    first.target.dispatchEvent(simulatedEvent);
    event.preventDefault();
}

function initTouchHandlers()
{
    document.addEventListener("touchstart", touchHandler, true);
    document.addEventListener("touchmove", touchHandler, true);
    document.addEventListener("touchend", touchHandler, true);
    document.addEventListener("touchcancel", touchHandler, true);    
}

function render(){
    requestAnimationFrame(render);
    controls.update();
    renderer.render(scene, camera);
}