//
//  Contour3D.swift
//  contourmApp
//
//  Created by mobiledev on 5/14/15.
//  Copyright (c) 2015 mobiledev. All rights reserved.
//

import UIKit

class ContourMap: UIViewController, UIGestureRecognizerDelegate{

    @IBOutlet var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let localfilePath = NSBundle.mainBundle().URLForResource("html/contourMap", withExtension: "html");
        let myRequest = NSURLRequest(URL: localfilePath!);
        webView.loadRequest(myRequest);
        webView.scrollView.scrollEnabled = false;
        webView.scrollView.bounces = false;
        
        self.navigationController!.interactivePopGestureRecognizer.delegate = self;
    }
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer!) -> Bool {
        return false;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}