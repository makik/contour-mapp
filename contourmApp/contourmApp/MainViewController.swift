//
//  ViewController.swift
//  contourmApp
//
//  Created by mobiledev on 5/10/15.
//  Copyright (c) 2015 mobiledev. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet var aboutButton:UIBarButtonItem!;
    
    let aboutTitle: String = "Carleton College \n Geology Department \n"
    
    let aboutText: String = "Jon Green, Kiley Maki, Sam Vinitsky \n\n This is an app intended to help students understand and practice concepts related to contour maps and lines, especially the relationship between 2D representations and their 3D counterparts. The main activity of this app allows users to draw contour lines of a 2D surface and specify their height, and will create a 3D representation from the input data."

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func displayAboutText(sender:UIBarButtonItem){
        var alert = UIAlertController(title: self.aboutTitle, message: self.aboutText, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
}

